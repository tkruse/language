# Inclusive language guide

This project collects and summarizes ideas on how certain ways of expression modify the reception of the message. In particular, how certain expressions may inadvertently express discriminating opinions about race, gender, LGBT, disabilities and other factors used to marginalize groups. Many expressions listed here may still be used in various contexts without causing issue, but not in other contexts.

Language is a product of culture, and culture has seen plenty of discriminatory movements come and go, all of which influenced the language. So typical language can be expected to contain undesired artifacts from culture, and it takes some effort to avoid replicating those artifacts.

The choice of words is only a small aspect of being affirmative and inclusive, as an example what you say is more important than how you say it (But typically people make less mistakes about that).

This article is not intended to make anyone change their expression to accomodate others, but only to collect insights on what unintended consequences careless speech could have.

This articles makes no claim of being complete, only to cover a broad range of concepts and examples.

## Why it matters

The main reasons to mind language is to prevent distractions from your message, and to help make the world a better place for all (if that is among your goals).

More specific reasons:

1. Avoiding to cause unnecessary offense and emotional triggers
2. Establishing an inclusive attitude and mood (and avoiding making people feel unwelcome)
3. Contributing to a forming society with less bias and discrimination (and avoiding discrimination)
4. Training yourself to review messages from a different angle

Regarding a society with less biases, these articles might better explain the motivation:

* https://www.kappanonline.org/deconstructing-pyramid-prejudice-shields/
* https://www.history.com/news/brown-v-board-of-education-doll-experiment

### Will modifying language achieve anything for marginalized groups?

Nobody has a crystal ball to tell how small changes to our language will impact the future.
One thing is certain however, only changing expressions but no other behaviors will not be enough to make significant change. The language is just a small part of the picture, and the easily detectable patterns are just a small part of language. So there is no delusion about avoiding non-inclusive of discriminatory language being a miracle cure to all unfairness in society.

However, most people will find it annoying to reconsider their language, and the repetitive annoyance can serve as a reminder to be more considerate or maginalized groups also in other areas of our behavior than choice of words.

## Inclusive/Unbiased language

Inclusive language aims to avoid offense and fulfill the ideals of egalitarianism by avoiding expressions that express or imply ideas that are sexist, racist, or otherwise biased, prejudiced, or denigrating to any particular group of people (and sometimes animals as well). https://en.wikipedia.org/wiki/Inclusive_language

* Avoid pejorative language (plain insults)
* Avoid racist language
* Avoid sexist/genderized language
* Avoid negative stereotypes
* Avoid "benevolent" stereotypes (see below)
* Avoid language that relates "black/dark" and "white/bright" with other qualities
* Avoid misgendering
* Avoid ableist language (implying people with disabilities are less useful)
* Avoid sizeist language (implying taller or longer is better)
* Avoid healthist language (implying people with diseases are less useful)
* Avoid lookist language (implying that objective beauty exist and beautiful people are better)
* Avoid classist language (implying people with money or education are better)
* Avoid right-handed preferist language
* Avoid US-centric or euro-centric language
* Avoid religious dominance (implying one religion is the right one)
* Avoid cultural appropriation (see below)
* Avoid victimizing language
* Avoid language trivializing historical events of human right violations (war, witchhunt, crusade, inquisition), a.k.a. socially charged expressions
* Avoid language trivializing medical diseases (like addiction, depression, depressive, OCD) or crimes (abuse, rape, domestic violence)
* Avoid context-specific switch to special language to avoid the above (speak to/about all people in the same ways always)

### Avoid cultural appropriation

This broadly means disrespectful or careless borrowing of important symbols from marginalized cultures.

Examples:

* skin-shaded emoticons of a different ethnicity than one's own
* dressing to imitate other culture (for mockery or fun)
* using identifying symbols of a group that one does not belong to
* getting tribal tattoos of another culture
* speaking with an artificial accent of another group

This topic can be quite complex, with certain exceptions possible for:

* fair use (similar to copyright)
* proper attribution
* distinguishing copied style from genuine style
* granted permission by representatives of a culture (a council, or a comission)
* art and education (when done fairly and respectfully)

It is also generally invalid to claim that marginalized cultures also misappropriate white culture, or that general cross-pollination of cultures (e.g. british people eating italian Pizza) makes other forms of appropriation ok. The issue is not copyright, but power imbalance and oppression, and not all cultural artifacts are important symbols for a culture.

### Avoid "benevolent" stereotypes

Reinforcing positive sexist views is just as bad as negative ones:

* "Professors are invited along with their wives and children"
* women are more patient, compassionate, good at cooking, beautiful
* men are stronger, more assertive, better at repairing technical things

And similar harmful positive beliefs exist about people from LGBT communities.

Same for racist beliefs:

* Asians tend to score well on standardized tests
* African Americans are athletic, musical, religious, and had strong family ties
* European Americans are intelligent, organized, independent, and financially well-off

https://en.wikipedia.org/wiki/Benevolent_prejudice

Rejecting benevolent prejudices does not equal assuming all groups are equal. It rather means that any statistical difference between groups, even if it existed, was less pronounced than the differences within each group. That also means any difference is not perceivable (without doing maths), and talking as if those differences were perceivable and meaningful exaggerates them unduly. It can also mean people are victims of selective perception or media biases, making them (erroneously) believe such differences are meaningful when reasoning about other people (making prejudices).

There is a grey area between prejudice and reasonable assumption, where the differences between groups are sufficiently pronounced to be observable in daily life, but it is generally safer to err on the side of caution.

### Avoid misgendering, respect sexual identification

Misgendering refers to actions that refer to people attributing to them a different gender than their stated gender. Some gender identities are male, female, gender-neutral and non-binary. In terms of language, gender-neutral and non-binary have several similarities, but are not always interchangeable.

So it is misgendering to refer to a transwoman as "he" or "they", if her choice is "she".

This applies to:

* Pronouns (also see glossary)
* Titles (Mr, Mrs, Mx)
* Relations (Parent, child), with problematic words like uncle, nephew, widow
* Jobtitles

Also this whole topic is subject to frequent changes and ambiguity, as the language usage tries to adapt to new concepts.

When using pronouns to refer to people, prefer to respect their wishes of the pronoun to be used, unless the preference appears unreasonable. Reasonable choices include "he", "she", "they", and several neologisms like "ey" or "ze". (Unreasonable choice examples might include "her majesty", "god")

Genderbread person:

* Gender identity (more man, more woman, neither, both, non-binary) transgender
* Gender expression (man-like, woman-like, combined, neutral)
* Bioligical(genetical) sex (male, female, special cases/intersex)
* Sexual attraction (towards masculin/feminine), gay, lesbian, heterosexual, asexual, pansexual
* Romantic attraction (towards masculin/feminine)

### Avoid wrong focus on person vs. trait

* When in doubt, put people first
  instead of “a blind woman” or “a woman salesperson,” use “a woman who is blind” or “a woman on our sales team.” However do not do that when the people strongly identify with their feature (same as not saying: a person who is male).
* Consider identifying traits
  Members of some communities prefer "identity-first language"; for example, that preference is common in autistic, blind, and Deaf communities. Capitalization of identities also may vary. Whenever possible, research and choose terms that respect the ways people in the communities identify.

### Avoiding triggering and unnecessarily violent terms

A triggering term is an unexpected use of an image unrelated to the main topic that may remind people in unwelcome ways of traumatic memories.

An unnecessarily violent term describes a non-violent process using language from war, to an audience this might appear as if violence might also be condoned in other contexts.


#### Use trigger warnings

When providing content that are likely to disturb readers, warn them.

Sexual Assault, Abuse, Child abuse/pedophilia/incest, Animal cruelty or animal death, Self-harm and suicide, Eating disorders, body hatred and fat phobia, Violence, Pornographic content, Kidnapping and abduction, Death or dying, Pregnancy/childbirth, Miscarriages/abortion, Blood, Mental illness and ableism, Racism and racial slurs, Sexism and misogyny, Classism, Islamophobia, Transphobia and transmisogyny, Homophobia and heterosexism

## Avoid bad behaviors

When a person from a dominant culture/group (in the context) talks to a person from another culture/group, things to avoid:

* any compliments that could imply other group is inferior on average: "You're pretty smart (for a woman)"
* any compliment them on conformity with the dominant group ("You don't look like a gay")
* try to set them up with another person of their group/culture (as if the 2 people belonged together)
* ask people where they are "really" from
* don't compliment people with disabilities on how great they do
* don't insinuate any non-conformity is objectively bothersome ("You're name is hard to pronounce", "It's hard to tell what gender you are")
* at work: ask about why they follow certain religious practices

## Avoid context-specific switch to special language

When writing about people in general and not individual person, usages of 'he' or 'she' can sometimes be removed by restructuring a sentence, instead of using 'they':

"A person must live here for 20 years before he may apply for permanent residence." vs. "A person must live here for 20 years before applying for permanent residence."

However, when talking to/about specific persons, don't use that technique, it's unnatural and makes them feel singled out.

## Avoid abbreviations, jargon, idioms and inside-jokes for audience which might not be familiar with them

To be inclusive to newcomers to your community, lowering the entry barries can be helpful. In language, entry barriers can be expressions that only members inside the community understand well. Also expressions that typically only people inside a group know can be a barrier to newcomers. 

This is also relevant to international communities with a large subset coming from one culture (e.g. US-americans using baseball-related idioms a lot).

However, for communications inside the group, such expressions may remain the most concise, understandable or engaging way to express an idea.
Also using such can be a way to share and spread culture, so for that sake completely avoiding the terms may not be attractive to many people.

Examples are:

* Abbreviation that are uncommon (DFTBA, HMU, ...)
* Jargon (Pushing the envelope, Cubicle farm, Cradle to grave)
* Latin expressions "a priori", "de jure", "per se", "ad acta"...
* Inside jokes: "It's the 2015-Washmachine incident all over again"
* Idioms (examples only to indicate how frequent this may happen):
  * Pass the buck
  * stick it to the man
  * The ship has sailed
  * Pearls before swine
  * Run it up the flagpole
  * Touch base
  * trouble brewing
  * begging the question
  * sold down the river
  * a chip on the shoulder
  * rain check
  * Achilles heel
  * Pyrrhic victory
  * striking the iron while it's hot
  * barking up the wrong tree
  * a dime a dozen
  * have ducks in a row
  * beat around the bush
  * cut some slack
  * elephant in the room
  * glad to see the back
  * not one's cup of tea
  * once in a blue moon
  * pull someone's leg
  * take something with a pinch of salt
  * the last straw
  * ...

## US-centrism and Euro-centrism

Because of historic periods of imperialism and colonialism, many language items focus on the US or Europe and relate to other countries or culture in relation to that. This is a form of discrimination. Similarly, using maps or globes where north is at the top or Europe is in the middle is not merely a harmless convention, but affects our perception of importance. In a similar way, using the common Mercator projection as a map inflates the size of the USA / north america (and russia) compared to other countries. While turning maps around (south at the top) is generally very confusing to viewers (same as using some other projections), this is still something to be aware of.

Examples for US/Eurocentricsm:

* Using "American" for anything in the USA, even though Countries like Canada, Mexico, Brazil and Argentina are also on continents called the Americas.
* Eastern/Western countries, the middle east: This is from a european point of view
* Down under, a project going south (downwards)
* "Southern countries / southern climate"



## Counter-arguments and Exceptions

This section tries to preset examples where consensus seems difficult and the reasons given for continued usage of an expression. This document should not be used as a guideline for any expression, and will not try to be consistent about flagging a given expression as unsuitable or suitable, just to provide commonly given arguments.

Technical terms: Some words may have been used in certain technical or legal contexts so much that some people will believe using any other term will harm understandability more than what is gained for inclusiveness, examples are:

* blind spot
* black box
* darknet
* whitespace
* eastern / western countries

Expressions seemingly not relating to negative / positive values:


* black hole
* dark matter
* blackbox / whitebox
* blackboard / whiteboard
* the White House
* White Christmas

## Examples and Translations

Common expressions that were commonly used, but may better be replaced by an alternative:

| bad  | reason | better  |
|---|---|---|
| nigger, chink, spic, latino | pejorative racist | black person, latinX, asian|
| 'itis for full stomach | pejorative racist ||
| asshole, slut, bitch, cunt, ho, chick, doll, tart, dick, fucker, wanker | pejorative sexist | bad, mean, selfish, ridiculous, insincere, bigot, unfair, superficial|
| bitchy, bossy, ditsy, feisty, frigid, hormonal, hysterical, menopausal, pushy, sassy, shrill | pejorative sexist | |
| cretin, derp, retard, moron, idiot, imbecile, simpleton, spaz, wacko | pejorative ableist | uneducated, unreflective, dense, ignorant|
| fag, dyke, queer, poofta, tranny | pejorative ||
| that is/looks gay, straight | implies gay==bad ||
| Homosexual || gay |
| 'he' or 'his' as default pronoun | genderized | they, their, them|
| 'he' or 'she' based on perception of that person || the stated gender of the person |
| 'they' for transpeople | | 'he' or 'she', depending on their self-identity|
| Mr. Mrs. Ms. | genderized| Mx|
| guys, men | genderized | folks, people, you all, y’all, and teammates |
| ladies/gals | sexist | women, folks, people, you all, y’all, and teammates |
| girl/girls for adults | sexist, ageist | women (when adult)  |
| females | sexist | women |
| grow up, past his prime, brat, spinster | ageist ||
| Ladies and gentlemen | Binary ||
| mankind, layman, man-made, man-up | genderized ||
| manpower, man hours, man-in-the-middle, the average man | genderized | human resources, person hours|
| man-made | genderized | artificial|
| policeman, postman, cameraman, businessman, salesman, chairman | genderized ||
| working mother, female pilot, male nurse, woman doctor, female professor|genderized, cleaning lady, actress||
| forefather | genderized | ancestor|
| housewife as job | sexist | it's a marital status only|
| wife/husband | | partner|
| mother tongue | genderized | native tongue|
| African-american| black person|
| indian || native american (unless India)|
| blackmail | black==bad | extortion|
| black sheep | black==bad | outcast|
| black monday | black==bad | (though "black friday" is ok)|
| dark ages, darknet, black humor, black death, somber mood, white lie, whitewash, bright idea | black==bad | |
| blacklist/whitelist | black==bad | allow/deny |
| black hat/white hat (hacker, CEO) | black==bad | ethical, unethical |
| handicapped | ableist | disabled |
| blind, deaf, dumb, mute, autistic, elderly, homeless, divorced ... person/man/woman/child | ableist | person/man/woman/child who is ... |
| dummy | ableist | placeholder, puppet, mock, unused |
| A mute, addict, alcoholic, divorcee, homeless | | person/man/woman/child who is ... |
| it's common sense | ableist | rational, economical, logical, conventional|
| lame, crippled | ableist | uncool |
| weak/strong reason, weakness, strength, powerful | ableist | good, bad, convincing. However many valid uses exist |
| crazy, bonkers, daft, insane/sane, lunatic, loony, maniac, nuts, retarded | ableist | reasonable, logical, rational, economical  |
| blind spot | potentially ableist ||
| overweight, obese, fat, skinny, petite | sizeist | curvy, bulky, heavyset |
| master/slave | trivializing crime| |
| grammar/...-Nazi | trivializing crime| |
| first/second class citizen | trivializing crime| |
| nitty-gritty | slavery background | |
| pet owner | | pet guardian/provider |
| third world country | | developping country |
| "America" for USA | US-centric | USA |
| Christian name | supports religious dominance | First name |
| Merry Christmas/Easter | supports religious dominance ||
| god bless, god forbid, thank god, hell, devilish, Easter Egg, cross to bear, holy grail | supports religious dominance||
| Mental Disability | ableist | struggling with a mental health issue |
| poor, unfortunate person | | person of lower income |
| afflicted, victim/sufferer of disease | victimizing | person having disease |
| bound/confined to a wheelchair | victimizing||
| commit suicide | accusatory | die by/perform suicide |
| minority groups || wrong when group among the largest groups | Marginalized group |
| normal, healthy, able-bodied person | ableist, lookist | typical |
| cute/beautiful/pretty/ugly concept | lookist | nice, neat, sweet, inspiring |
| groomed | triggering metaphor ||
| oriental | pejorative in the US | asian|
| Using different speech form when talking with/about people of one group, e.g. avoiding pronouns | discrimination | use same speech form |
| long time no see, no can do | mocks non-english speakers ||
| Product names or expressions using: ninja, kamikaze, banzai, guru, yoga, karma, zen, nirvana, kosher, voodoo, viking, valhalla, spirit animal, tipi, pow wow|cultural appropriation ||
| tribal knowledge | prejudiced about tribal lifestyle, avoid the word tribe in general | inside-knowledge |
| suicide mission | evoking violence ||
| death march | evoking violence ||
| shoot down something | evoking violence ||
| nuke something | evoking violence ||
| ghetto | evoking violence ||
| deadline | evoking violence ||
| sudden death | evoking violence ||
| kill switch | evoking violence ||
| killer application | evoking violence ||
| thread starvation | evoking violence ||
| fire a person | evoking violence ||
| overkill | evoking violence ||
| harakiri, kamikaze | evoking violence ||
| russian roulette | evoking violence ||
| STONITH, STOMITH | evoking violence ||
| kill time | evoking violence ||
| suffocating, poisonous, toxic | evoking violence ||

Idioms and phrases (examples)

| bad  | reason | better  |
|---|---|---|
| like a girl/pussy | sexist ||
| grow a pair | sexist ||
| boys will be boys | sexist ||
| wearing the pants | sexist ||
| Who's your daddy now | sexist ||
| don't shoot the messenger | evoking violence ||
| receive a whipping | evoking violence ||
| get your ass kicked | evoking violence ||
| shoot from the hip | evoking violence ||
| break a leg | evoking violence ||
| roll with the punches | evoking violence ||
| cut-throat | evoking violence ||
| stab in the back | evoking violence ||
| no silver bullet | evoking violence ||
| poison the well | evoking violence ||
| over my dead body | evoking violence ||
| from my cold dead hands| evoking violence ||
| rather die in a ditch | evoking violence ||
| take a bullet for the team | evoking violence ||
| bite a bullet | evoking violence ||
| shoot yourself in the foot | evoking violence ||
| blow up in yor fence | evoking violence ||
| caught a lot of flak | evoking violence ||
| the nuclear option | evoking violence ||
| to go ballistic | evoking violence ||
| blow something out of the water | evoking violence ||
| kill 2 birds with one rock | evoking violence ||
| curiosity killed the cat | evoking violence ||
| dog eat dog | evoking violence ||
| bite the hand that feeds you | evoking violence ||
| like a headless chicken | evoking violence ||
| like a lamb to the slaughter | evoking violence ||
| More than one way to skin a cat | evoking violence ||
| that's suicide | evoking violence ||
| makes me want to kill myself | evoking violence ||
| Skeleton in the closet | evoking violence ||

## Resources

* https://aso-resources.une.edu.au/academic-writing/usage/non-discriminatory-language/
* http://supp.apa.org/style/pubman-ch03-00.pdf
* https://sproutsocial.com/seeds/writing/inclusive-language/
* https://www.vic.gov.au/inclusive-language-guide
* https://medium.com/diversity-together/70-inclusive-language-principles-that-will-make-you-a-more-successful-recruiter-part-2-eb0f5e4f6e80
* https://www.borusan.com/Assets/Media/PDF/BorusanEsittir_Rehber-En-2.pdf
* http://www.humber.ca/makingaccessiblemedia/modules/01/transript/Inclusive_Language_Guide_Aug2017.pdf
* https://ncdj.org/style-guide/
* https://www.autistichoya.com/p/ableist-words-and-terms-to-avoid.html
* https://en.wikipedia.org/wiki/List_of_disability-related_terms_with_negative_connotations
* https://medium.com/diversity-together/70-inclusive-language-principles-that-will-make-you-a-more-successful-recruiter-part-1-79b7342a0923
* https://www.linguisticsociety.org/resource/guidelines-inclusive-language
* http://sacraparental.com/2016/05/14/everyday-misogyny-122-subtly-sexist-words-women/
* https://developers.google.com/style/inclusive-documentation

## Glossary

### Ableism

Practices and dominant attitudes in society that assume there is an ideal body and mind that is better than all others. Examples for ableist phrases:

* "blind to the consequences"
* turn a blind eye
* follow blindly
* blind spot
* "falls on deaf ears"
* "stand up for yourself"
* "eye-opener"
* being "all ears"
* play by ear
* "standing your ground"
* upstanding/upright citizen
* crooked politician
* twisted plans
* just sitting there
* make your voice heard
* peerson able to think on their feet

However, other metaphors that do not equate ability with positive or negatice attribute may still be used, even when talking to or about people which may not be suitable for that metaphor (unless obviously when selecting metaphors specifically to mock someone)

* go for a walk
* take a step back
* point the finger
* get a foot in the door
* see eye to eye
* jump to conclusions
* wrongfooting

### Accessibility

The practice of designing and developing web sites and web content that provide a great experience for all web users

### Ageism

A system of beliefs, attitudes and actions that stereotype and discriminate against individuals or groups on the basis of their age

### Ally

Someone who supports a group other than their own (in terms of racial identity, gender, faith identity, sexual orientation, etc.) Allies acknowledge disadvantage and oppression of other groups; take risks and action on the behalf of others; and invest in strengthening their own knowledge and awareness of oppression

### Affinity groups

A group of people who choose to meet to explore a shared identity such as race, gender, age, religion, and sexual orientation. These groups can be further broken down into smaller groups within the two major affinities (i.e. women, LGBTQIA, non-binary, African American men/women, bi/multi-racial, etc.). At work these are sometimes called Employee Resource Groups, or ERGs

###  Cisgender

Individuals whose gender identity aligns with their birth assignment

### Diversity

Individual differences that include (but not limited to) ability, learning styles, life experiences, race and ethnicity, socioeconomic status, gender, sexual orientation, country of origin, political, and religion.

### Dominant culture

The cultural beliefs, values, and traditions that are centered and dominant in society’s structures and practices. Dominant cultural practices are thought of as “normal” and, therefore, preferred and right. As a result, diverse ways of life are often devalued, marginalized and associated with low cultural capital. Conversely, in a multicultural society, various cultures are celebrated and respected equally.

### Equity

Equity aims at making sure that everyone’s lifestyle is equal even if it may come at the cost of unequal distribution of access and goods. Social justice leaders in education strive to ensure equitable outcomes for their students.

### Equality

Everyone is given equal opportunities and accessibility and are then free to do what they please with it. However, this is not to say that everyone is then inherently equal. Some people may choose to seize these open and equal opportunities while others let them pass by.

### Etymology

Etymology is the study of the history of words. When discussing whether a word is appropriate or not, often etymology is used. Note that not every argument based on etymology is valid. Just because a word was once meant or used in one way, does not mean it still has that meaning today.

### Inclusion

A dynamic state of operating in which diversity is leveraged to create a fair, healthy and high-performing organization or community. An inclusive environment ensures equitable access to resources and opportunities for all. It also enables individuals and groups to feel safe, respected, engaged, motivated and valued, for who they are and for their contributions toward organizational and societal goals.

### LGBTQIA

Acronym encompassing the diverse groups of lesbian, gay, bisexual, transgender, queer, intersex and asexual populations. Sometimes LGBTQ+, or just LGBT.

Avoid defaulting to umbrella terms like gay or homosexual. Use LGBTQIA to refer to a broad community or be specific when relevant: lesbian, gay man, bisexual woman, etc.

When referring to the broader community, queer (as in queer people) or LGBTQIA (as in LGBTQIA people) is appropriate – gay, however, is not. LGBTQIA is only appropriate when referring to the broader community or groups of people, not when referring to individuals.

### Meritocracy

Belief in the (flawed) idea that hard work and talent alone are all that’s needed to achieve success. Challenges like implicit bias, structural inequality and varying degrees of privilege or disadvantage mean meritocracy isn’t currently a reality.

### Microaggression

This term was coined by psychiatrist and Harvard University professor Chester M. Pierce in 1970 to describe the tiny, casual, almost imperceptible insults and degradation often felt by any marginalized group.

### Non-binary

Any gender identity that does not fit the socially-constructed male and female binary

### Pejorative

A pejorative (also called a derogatory term,[1] a slur, a term of disparagement) is a word or grammatical form expressing a negative connotation or a low opinion of someone or something, showing a lack of respect for someone or something.

### POC

An acronym standing for “person of color”

### Pronoun

A consciously chosen set of pronouns that allow a person to represent their gender identity accurately. Pronouns include both gendered pronouns like “He” and “She” as well as gender-neutral pronouns like “They” and “Ze.”

Sizeism or size discrimination is the idea that people are prejudged by their size or body shape.

New pronoun creations:

| Name | Nominative  | Objective  | Possessive  determiner | possessive    | reflexive |
|---|---|---|---|---|---|
| Ne |  Ne laughed |  I called nem |  Nir eyes gleam |  That is nirs   |  Ne likes nemself |
| Ve |  Ve laughed |  I called ver |  Vis eyes gleam |  That is vis    |  Ve likes verself |
| Ey |  Ey laughed |  I called em  |  Eir eyes gleam |  That is eirs   |  Ey likes emself |
| Ze (or zie) and hir | Ze laughed |  I called hir   |  Hir eyes gleam |  That is hirs | Ze likes hirself |
| Ze (or zie) and zir | Ze laughed |  I called zir   |  Zir eyes gleam |  That is zirs | Ze likes zirself |
| Xe |  Xe laughed |  I called xem |  Xyr eyes gleam |  That is xyrs   |	Xe likes xemself |



### Underrepresented group/under indexed group

This term describes any subset of a population that holds a smaller percentage within a significant subgroup than it holds in the general population. Women are often an underrepresented group in science, technology, engineering, and mathematics, for example.
Exclusive terms and phrases to avoid

### Guys

This is gendered language, and should be avoided when addressing a co-ed group.
Replace with: Gender-neutral language such as: folks, people, you all, etc.

### Girls/ladies/gals

Women over 18 are not “girls,” while “ladies” and “gals” are both potentially patronizing.
Replace with: Women

### Handicap/handicapped

Disability rights activists question the use of these terms. In this case, we default to preferred terms.
Replace with: disabled, person with a disability

### Impaired (ie. hearing impaired/visually impaired, etc)

While not every person who is deaf or blind takes issue with the term “impaired,” it may be best to avoid. The Merriam-Webster dictionary defines “impaired” as “being in a less than perfect or whole condition: as disabled or functionally defective.” To suggest that people who can not hear or see–or have difficulties hearing or seeing–are less than whole is neither inclusive or empathetic–and thus, not Sprout. The term also suggests impermanence, that “impairments” can be fixed–which is also not always the case.

### Indian/American Indian

This language dates back to Christopher Columbus and naming a people on a Anglo-Saxon perception. It implies that these nations are only defined by how they were perceived by Europeans after 1492, when their people were massacred.
Replace with: native, indigenous

### Lame

Insensitive term that was originally used to reference people with reduced mobility, then became a synonym for “uncool.” Neither use is acceptable.
Replace with: person with reduced mobility, disabled, person with a disability

### Mental disability/mentally handicapped

Rarely is this characterization necessary in our writing. In the rare case it is relevant to the story, always be sure to use people-first language, and be specific with your description–you don’t want to lump all diagnoses together. This is also another opportunity for self-identification.

In addition, avoid using phrases like, “suffers from” or “victim of” because it assumes that individual is suffering or identifies as a victim–which may not accurately describe their experience. Avoid any and all language that connotes pity.
Replace with: Person who has schizophrenia, person who has depression

### Minority

Not all marginalized groups are minorities, and a broader term is generally inclusive of more than race and gender. Also, the use of “minority” may imply inferior social position.
Replace with: underrepresented groups, marginalized groups

## PC Police

From wikipedia (https://en.wikipedia.org/wiki/Political_correctness)
Political correctness is a term used to describe language, policies, or measures that are intended to avoid offense or disadvantage to members of particular groups in society. The term "PC police" is generally used as a pejorative with an implication that these policies are excessive or unwarranted.

It might be wise to be careful what you say yourself, but to not assume others were deliberately offensive or careless in their choice of words.

### Sexual preference, preferred pronoun

Again, not something that will come up in external communication often, but important to caution against the use of this phrase. The word “preference” implies a choice, which–for many members of the LGBTQIA community–is not accurate.
Replace with: sexual orientation, stated pronoun
